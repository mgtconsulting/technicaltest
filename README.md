# Log Parser Module

 This library implements methods to process connection logs of the form:
 
 >1366815793 gandalf tooler
 >
 >1366815795 higgins electron
 >
 >1366815811 sbmaster01 sbrma01
 
 where first column is timestamp, second is origin host and third is destination host
 
 This class holds methods to extract 3 diferent queries from them:
 
 - a list of hostnames connected to a given (configurable) host during the last hour.
 - a list of hostnames received connections from a given (configurable) host during the last hour.
 - the hostname that generated most connections in the last hour.
 
 
 ## Functions
 
 ### - load_log ( spark, path)
 
 This method is just a shortcut for reading the logs as a spark dataframe with the proper default schema and delimiter.
 Returns a Dataframe.
 
 Schema: 
 
 >**timestamp** - IntegerType
 >
 >**host_a** - StringType
 >
 >**host_b** - StringType
 
 Delimiter: " "
 
 Paremeters: 

   - spark: Spark Session

   - path : Full path to the directory where the logs are being stored
   
     Example: "/dbfs/mnt/log_storage/"
     
### - connections_to_host ( spark, df , hostname)

This method returns the list of hostnames connected to a given host during the last hour, as a Dataframe.

Paremeters: 

 - spark: Spark Session

 - df : The dataframe holding the log information. It expects to have the schema specified by the load_log function.


 - hostname:  hostname to filter the query.

### - connections_from_host ( spark, df , hostname)

This method returns the list of hostnames that received connections from a given host during the last hour, as a Dataframe.

Paremeters: 

 - spark: Spark Session

 - df : The dataframe holding the log information. It expects to have the schema specified by the load_log function.


 - hostname:  hostname to filter the query.
   
   
### - most_active_host ( spark, df )

This method returns the hostname that generated most connections during the last hour, as a Dataframe.

Paremeters: 

 - spark: Spark Session

 - df : The dataframe holding the log information. It expects to have the schema specified by the load_log function.


 ## Usage
 
 To make use of this class, import it like this: 

 >from logparserlib import logfunctions
 
then get your spark session, we will need to pass it to the functions.

> spark = SparkSession.builder.getOrCreate()
> 
> df = logfunctions.load_log(spark, "/your/path/to/logs")
> 
> logfunctions.connections_to_host(spark,df,"Your-hostname").show()
 
 
 ## Building the library

You can use the environment.yml included in this project to create a clean new environment for building pourposes.
For example with conda:

>conda env create -f environment.yml

Otherwise import these libraries:

    pip install wheel
    pip install setuptools
    pip install twine

then run inside the project folder:

> python setup.py bdist_wheel

The wheel file is stored in the “dist” folder that is now created.
Next, change to your working environment where you want to use this library and run:

>pip install /path/to/wheelfile.whl

 ## Final Note
 To run this notebook indefinetely in Databricks, like the challenge proposes, We recommend using the Jobs Option.
 Scheduling the notebook to run once an hour.
 
 Otherwhise, if running this notebook outside Databricks Try using a similar tool like a Cron scheduler.
 Or do something like this:
 
    not_end = True
    while (not_end):
      df = logfunctions.load_log(spark, "/your/path/to/logs")
      logfunctions.connections_to_host(df,log_parser_config["hostname"]).show()
      logfunctions.connections_from_host(df,log_parser_config["hostname"]).show()
      logfunctions.most_active_host(df).show()
      t.sleep(3600)