from pyspark.sql.types import StringType, StructField, StructType, IntegerType, TimestampType
import time as t


  
def load_log( spark, path = ""):
    mySchema = StructType(
    [StructField("timestamp", IntegerType(), True),\
    StructField("host_a", StringType(), True),\
    StructField("host_b", StringType(), True),\
    ])
    return spark.read.schema(mySchema).option("delimiter", " ").csv(path)

def connections_to_host(spark, df,hostname ="" ):
    df.createOrReplaceTempView("connections")
    return spark.sql("select *  from connections where host_b = '"+ hostname + "' and timestamp >=  "+ str(int(t.time()) - 3600))

def connections_from_host(spark, df,hostname ="" ):
    df.createOrReplaceTempView("connections")
    return spark.sql("select *  from connections where host_a = '"+ hostname + "' and timestamp >=  "+ str(int(t.time()) - 3600))

## Note: if logs hold a lot of diferent hostnames and this query takes too much time, 
## it could be potentially speed up by changing the "order by" implementation with a "Max" function
def most_active_host(spark, df):
    df.createOrReplaceTempView("connections")
    return spark.sql("select host_a , count(host_a) as connections from connections where timestamp >= "+  str(int(t.time()) - 3600)+" group by host_a order by host_a desc limit 1")

