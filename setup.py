from setuptools import find_packages, setup

setup(
    name='logparserlib',
    packages=find_packages(include=['logparserlib']),
    version='0.1.0',
    description='Challenge Log parser library',
    author='Juan Diego',
    license='MIT',
    install_requires=[
        'pyspark',
      ],
)